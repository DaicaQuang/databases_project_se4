/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectdatabase;

import java.sql.*;
import java.util.*;

/**
 *
 * @author DaicaQuang
 */
public class SQLProjectTest {

    public static void main(String[] args) throws SQLException, InterruptedException {
        Scanner scanner1 = new Scanner(System.in);
        Scanner scanner2 = new Scanner(System.in);
        boolean correctLogin = false;
        int keuze = 0;
        boolean fouteKeuze = true;
        Connection sql = null;

        while (fouteKeuze) {
            try {
                System.out.println("Kies MySQL-server (1) of MSSQL-server (2)");
                keuze = scanner1.nextInt();
                if (keuze != 1 && keuze != 2) {
                    System.out.println("Er moet een 1 of 2 ingevoerd worden");
                } else {
                    fouteKeuze = false;
                }
            } catch (InputMismatchException exception) {
                System.out.println("Er is geen getal ingevoerd");
                System.out.println("Voer een geldig getal in. ");
            }
        }
        if (keuze == 1) {
            Connection mySQL = DriverManager.getConnection("jdbc:mysql://Localhost/testjava", "root", "BroodjeKaas");
            System.out.println("MySQL-server is gekozen (optie 1)");

//          Prepared Statement
            PreparedStatement query;
            query = mySQL.prepareStatement("SELECT voornaam FROM persoon");
            ResultSet result = query.executeQuery();

            while (result.next()) {
                System.out.println(result.getRow() + ": ");
                System.out.println(result.getString("voornaam") + "\t");
            }

            mySQL.close();
        }

        if (keuze == 2) {
            Connection MSSQL = DriverManager.getConnection("jdbc:sqlserver://localhost:1433;databaseName=AuditBlackBox;integratedSecurity=true;");
            System.out.println("MSSQL-server is gekozen (optie 2)");

//            Normale statement
//            Statement stat1 = MSSQL.createStatement();
//            String querySelect = "SELECT GeboorteDatum FROM dbo.Persoon";
//            ResultSet result = stat1.executeQuery(querySelect);
            // PreparedStatement
            PreparedStatement query;
            query = MSSQL.prepareStatement("SELECT GeboorteDatum FROM dbo.Persoon");
            ResultSet result = query.executeQuery();

            while (result.next()) {
                System.out.println(result.getRow() + ": ");
                System.out.println(result.getDate("GeboorteDatum") + "\t");
            }
            MSSQL.close();
            result.close();
        }
    }
}
