package projectdatabase;

import java.sql.*;

/**
 *
 * @author DaicaQuang
 */
public class SQLCreate {

    public static void main(String[] args) throws SQLException {
        Connection sql = DriverManager.getConnection("jdbc:mysql://Localhost/testjava", "root", "BroodjeKaas");

        PreparedStatement query;
        query = sql.prepareStatement("CREATE TABLE persoon("
                + "voornaam varchar(30),"
                + "achternaam varchar(30),"
                + "woonplaats varchar(50),"
                + "leeftijd int);");

        query = sql.prepareStatement("INSERT INTO persoon (voornaam, achternaam, woonplaats, leeftijd)"
                + "VALUES ('Mark', 'Jefferson', 'Oregon', '25');");
        query.execute();
        sql.close();
    }

}
